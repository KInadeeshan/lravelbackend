<?php
use App\Models\person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/person',function(){
    return person::all();
});
 route::post('/people',function(){
    $result = person::create([
        'userName'   => request('userName'),
        'password'   => md5(request('password')),
        'fullName'   => request('fullName'),
        'adress'     => request('adress'),
        'mobile'     => request('mobile'),
        'email'      => request('email'),
    ]);
    
    if($result != null){
        return response()->json(['success' => true,], 201);
    }
    else{
        return response()->json(['success' => false,], 201);
    }

 });

 route::post('/checkUser', function() {
     $usernameList = ["Ishara", "Dilusha", "Pavan", "Rusiru"];
    $username = request("username");
    if(in_array($username, $usernameList)) {
        return response()->json(['success' => true,], 201);
    }
    return response()->json(['success' => false,], 201);
 });

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


